function s=diag_sum(Q)
[a,b]=size(Q);
c=min(a,b);
d=(c+1)/2;
Q=Q([1:c],[1:c]);
P=flip(Q);
if mod(c,2)==0
     s=sum(diag(Q))+sum(diag(P));
else
     s=sum(diag(Q))+sum(diag(P))-Q(d,d);
end
end